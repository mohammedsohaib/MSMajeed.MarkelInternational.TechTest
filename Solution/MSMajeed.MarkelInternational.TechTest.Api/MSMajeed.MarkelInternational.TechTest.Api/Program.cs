using Microsoft.EntityFrameworkCore;
using MSMajeed.MarkelInternational.TechTest.Data;
using MSMajeed.MarkelInternational.TechTest.Data.Services;
using MSMajeed.MarkelInternational.TechTest.Data.Services.Interfaces;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add database context
builder.Services.AddDbContextFactory<TechTestDbContext>(options => options.UseInMemoryDatabase("TechTestDb"));

// Add dependencies
builder.Services.AddSingleton<ICompanyService, CompanyService>();
builder.Services.AddSingleton<IClaimService, ClaimService>();

// Lowercase routing
builder.Services.AddRouting(options => options.LowercaseUrls = true);

// Add services to the container
builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(o => o.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml")));

// Add AutoMapper
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

var app = builder.Build();

// Seed in memory database
using (var dbContext = app.Services.GetService<IDbContextFactory<TechTestDbContext>>().CreateDbContext())
{
    dbContext.Database.EnsureCreated();
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors(policy => policy.AllowAnyHeader()
                            .AllowAnyMethod()
                            .SetIsOriginAllowed(origin => true)
                            .AllowCredentials());

app.UseAuthorization();

app.MapControllers();

app.Run();
