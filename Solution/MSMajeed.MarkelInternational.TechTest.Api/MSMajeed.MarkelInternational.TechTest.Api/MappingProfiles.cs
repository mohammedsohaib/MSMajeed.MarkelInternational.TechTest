﻿using AutoMapper;
using MSMajeed.MarkelInternational.TechTest.Api.Models;
using MSMajeed.MarkelInternational.TechTest.Data.Models;

namespace MSMajeed.MarkelInternational.TechTest.Api
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<UpdateClaimModel, Claim>();
            CreateMap<Company, CompanyDetail>();
            CreateMap<Claim, ClaimDetail>();
        }
    }
}
