using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MSMajeed.MarkelInternational.TechTest.Api.Models;
using MSMajeed.MarkelInternational.TechTest.Data.Models;
using MSMajeed.MarkelInternational.TechTest.Data.Services.Interfaces;

namespace MSMajeed.MarkelInternational.TechTest.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ClaimsController : ControllerBase
    {
        private readonly ILogger<ClaimsController> _logger;
        private readonly IClaimService _claimService;
        private readonly ICompanyService _companyService;
        private readonly IMapper _mapper;

        public ClaimsController(ILogger<ClaimsController> logger, IClaimService claimService, ICompanyService companyService,
            IMapper mapper)
        {
            _logger = logger;
            _claimService = claimService;
            _companyService = companyService;
            _mapper = mapper;
        }

        /// <summary>
        /// Updates the claim.
        /// </summary>
        /// <param name="claimId">The claim identifier.</param>
        /// <param name="updateClaimModel">The update claim model.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{claimId}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UpdateClaim(int claimId, UpdateClaimModel updateClaimModel)
        {
            try
            {
                if (claimId < 1)
                {
                    return BadRequest($"Invalid claimId:{claimId}. Provide valid claimId");
                }

                if (_claimService.GetClaim(claimId) == null)
                {
                    return NotFound($"No claim found with the claimId:{claimId}");
                }

                if (updateClaimModel == null)
                {
                    return BadRequest($"updateClaimModel cannot be null. Please provide valid data");
                }

                if (!ModelState.IsValid)
                {
                    var modelErrors = string.Join(",", ModelState.SelectMany(x => x.Value.Errors).Select(e => e.ErrorMessage));
                    return BadRequest($"Invalid model, please verify the data: {modelErrors}");
                }

                if (await _companyService.GetCompany(updateClaimModel.CompanyId) == null)
                {
                    return NotFound($"Cannot find company with companyId:{updateClaimModel.CompanyId}");
                }

                if (await _claimService.GetClaimType(updateClaimModel.ClaimTypeId) == null)
                {
                    return NotFound($"Cannot find claim type with claimTypeId:{updateClaimModel.ClaimTypeId}");
                }

                var updatedClaim = _mapper.Map<Claim>(updateClaimModel);
                updatedClaim.Id = claimId;

                await _claimService.UpdateClaim(updatedClaim);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Gets the claim.
        /// </summary>
        /// <param name="claimId">The claim identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{claimId}")]
        [ProducesResponseType(typeof(ClaimDetail), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetClaim(int claimId)
        {
            try
            {
                if (claimId < 1)
                {
                    return BadRequest($"Invalid claimId:{claimId}. Provide valid claimId");
                }

                var claim = await _claimService.GetClaim(claimId);
                if (claim == null)
                {
                    return NotFound($"Claim not found with claimId:{claimId}");
                }

                return Ok(_mapper.Map<ClaimDetail>(claim));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}