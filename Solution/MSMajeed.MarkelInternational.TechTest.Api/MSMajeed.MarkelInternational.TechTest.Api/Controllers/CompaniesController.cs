using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MSMajeed.MarkelInternational.TechTest.Api.Models;
using MSMajeed.MarkelInternational.TechTest.Data.Models;
using MSMajeed.MarkelInternational.TechTest.Data.Services.Interfaces;

namespace MSMajeed.MarkelInternational.TechTest.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CompaniesController : ControllerBase
    {
        private readonly ILogger<CompaniesController> _logger;
        private readonly ICompanyService _companyService;
        private readonly IMapper _mapper;

        public CompaniesController(ILogger<CompaniesController> logger, ICompanyService companyService, IMapper mapper)
        {
            _logger = logger;
            _companyService = companyService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets the companies.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Company>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetCompanies()
        {
            try
            {
                var companies = await _companyService.GetCompanies();
                return Ok(companies);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Gets the company.
        /// </summary>
        /// <param name="companyId">The company identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{companyId}")]
        [ProducesResponseType(typeof(CompanyDetail), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetCompany(int companyId)
        {
            try
            {
                if (companyId < 1)
                {
                    return BadRequest($"Invalid companyId:{companyId}. Provide valid companyId");
                }

                var company = await _companyService.GetCompany(companyId);
                if (company == null)
                {
                    return NotFound($"Company not found with companyId:{companyId}");
                }

                return Ok(_mapper.Map<CompanyDetail>(company));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Gets the company claims.
        /// </summary>
        /// <param name="companyId">The company identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{companyId}/claims")]
        [ProducesResponseType(typeof(IEnumerable<Claim>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetCompanyClaims(int companyId)
        {
            try
            {
                if (companyId < 1)
                {
                    return BadRequest($"Invalid companyId:{companyId}. Provide valid companyId");
                }

                if (await _companyService.GetCompany(companyId) == null)
                {
                    return BadRequest($"Company not found with companyId:{companyId}");
                }

                var companyClaims = await _companyService.GetCompanyClaims(companyId);
                return Ok(companyClaims);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}