﻿using System.ComponentModel.DataAnnotations;

namespace MSMajeed.MarkelInternational.TechTest.Api.Models
{
    public class UpdateClaimModel
    {
        /// <summary>
        /// Gets or sets the ucr.
        /// </summary>
        /// <value>
        /// The ucr.
        /// </value>
        [MaxLength(20)]
        public string? UCR { get; set; }

        /// <summary>
        /// Gets or sets the company identifier.
        /// </summary>
        /// <value>
        /// The company identifier.
        /// </value>
        [Required]
        public int CompanyId { get; set; }

        /// <summary>
        /// Gets or sets the claim type identifier.
        /// </summary>
        /// <value>
        /// The claim type identifier.
        /// </value>
        [Required]
        public int ClaimTypeId { get; set; }

        /// <summary>
        /// Gets or sets the claim date.
        /// </summary>
        /// <value>
        /// The claim date.
        /// </value>
        public DateTime? ClaimDate { get; set; }

        /// <summary>
        /// Gets or sets the loss date.
        /// </summary>
        /// <value>
        /// The loss date.
        /// </value>
        public DateTime? LossDate { get; set; }

        /// <summary>
        /// Gets or sets the name of the assured.
        /// </summary>
        /// <value>
        /// The name of the assured.
        /// </value>
        [MaxLength(100)]
        public string? AssuredName { get; set; }

        /// <summary>
        /// Gets or sets the incurred loss.
        /// </summary>
        /// <value>
        /// The incurred loss.
        /// </value>
        public decimal? IncurredLoss { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Claim"/> is closed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if closed; otherwise, <c>false</c>.
        /// </value>
        public bool Closed { get; set; }
    }
}
