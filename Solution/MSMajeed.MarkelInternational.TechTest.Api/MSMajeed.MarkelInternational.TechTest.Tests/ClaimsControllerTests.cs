using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using MSMajeed.MarkelInternational.TechTest.Api.Controllers;
using MSMajeed.MarkelInternational.TechTest.Api.Models;
using Xunit;

namespace MSMajeed.MarkelInternational.TechTest.Tests
{
    public class ClaimsControllerTests
    {
        [Fact]
        public async Task GetClaimTest()
        {
            using (var testSetup = new TestSetup())
            {
                var mockClaimsControllerLogger = new Mock<ILogger<ClaimsController>>();
                var claimsController = new ClaimsController(mockClaimsControllerLogger.Object, testSetup.mockClaimService, testSetup.mockCompanyService, testSetup.mockAutoMapper);

                var actionResult = await claimsController.GetClaim(1);
                Assert.NotNull(actionResult);

                var okObjectResult = actionResult as OkObjectResult;
                Assert.NotNull(okObjectResult);
                Assert.True(okObjectResult.StatusCode == StatusCodes.Status200OK);

                var claim = okObjectResult.Value as ClaimDetail;
                Assert.NotNull(claim);
                Assert.True(claim.Id == 1);
            }
        }

        [Fact]
        public async Task UpdateClaimTest()
        {
            using (var testSetup = new TestSetup())
            {
                var mockClaimsControllerLogger = new Mock<ILogger<ClaimsController>>();
                var claimsController = new ClaimsController(mockClaimsControllerLogger.Object, testSetup.mockClaimService, testSetup.mockCompanyService, testSetup.mockAutoMapper);

                var getClaimActionResult = await claimsController.GetClaim(1);
                var getClaimOkObjectResult = getClaimActionResult as OkObjectResult;
                var claim = getClaimOkObjectResult.Value as ClaimDetail;

                var updateClaimModel = new UpdateClaimModel
                {
                    AssuredName = claim.AssuredName,
                    ClaimDate = claim.ClaimDate,
                    ClaimTypeId = claim.ClaimTypeId,
                    Closed = claim.Closed,
                    CompanyId = claim.CompanyId,
                    IncurredLoss = claim.IncurredLoss,
                    LossDate = claim.LossDate,
                    UCR = "Test"
                };

                var updateClaimActionResult = await claimsController.UpdateClaim(1, updateClaimModel);
                Assert.NotNull(updateClaimActionResult);

                var updateClaimOkResult = updateClaimActionResult as OkResult;
                Assert.NotNull(updateClaimOkResult);
                Assert.True(updateClaimOkResult.StatusCode == StatusCodes.Status200OK);

                var getUpdatedClaimActionResult = await claimsController.GetClaim(1);
                var getUpdatedClaimOkObjectResult = getUpdatedClaimActionResult as OkObjectResult;
                var updatedClaim = getUpdatedClaimOkObjectResult.Value as ClaimDetail;
                Assert.True(updatedClaim.UCR == updateClaimModel.UCR);
                Assert.True(updatedClaim.AssuredName == updateClaimModel.AssuredName);
                Assert.True(updatedClaim.ClaimDate == updateClaimModel.ClaimDate);
                Assert.True(updatedClaim.ClaimTypeId == updateClaimModel.ClaimTypeId);
                Assert.True(updatedClaim.Closed == updateClaimModel.Closed);
                Assert.True(updatedClaim.CompanyId == updateClaimModel.CompanyId);
                Assert.True(updatedClaim.IncurredLoss == updateClaimModel.IncurredLoss);
                Assert.True(updatedClaim.LossDate == updateClaimModel.LossDate);
            }
        }
    }
}