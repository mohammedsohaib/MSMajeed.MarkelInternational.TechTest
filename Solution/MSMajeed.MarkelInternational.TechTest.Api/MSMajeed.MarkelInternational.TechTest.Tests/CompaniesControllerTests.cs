using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using MSMajeed.MarkelInternational.TechTest.Api.Controllers;
using MSMajeed.MarkelInternational.TechTest.Api.Models;
using MSMajeed.MarkelInternational.TechTest.Data.Models;
using Xunit;

namespace MSMajeed.MarkelInternational.TechTest.Tests
{
    public class CompaniesControllerTest
    {
        [Fact]
        public async Task GetCompanyTest()
        {
            using (var testSetup = new TestSetup())
            {
                var mockCompannyControllerLogger = new Mock<ILogger<CompaniesController>>();
                var companiesController = new CompaniesController(mockCompannyControllerLogger.Object, testSetup.mockCompanyService, testSetup.mockAutoMapper);

                var actionResult = await companiesController.GetCompany(1);
                Assert.NotNull(actionResult);

                var okObjectResult = actionResult as OkObjectResult;
                Assert.NotNull(okObjectResult);
                Assert.True(okObjectResult.StatusCode == StatusCodes.Status200OK);

                var company = okObjectResult.Value as CompanyDetail;
                Assert.NotNull(company);
                Assert.True(company.Id == 1);
            }
        }

        [Fact]
        public async Task GetCompaniesTest()
        {
            using (var testSetup = new TestSetup())
            {
                var mockCompannyControllerLogger = new Mock<ILogger<CompaniesController>>();
                var companiesController = new CompaniesController(mockCompannyControllerLogger.Object, testSetup.mockCompanyService, testSetup.mockAutoMapper);

                var actionResult = await companiesController.GetCompanies();
                Assert.NotNull(actionResult);

                var okObjectResult = actionResult as OkObjectResult;
                Assert.NotNull(okObjectResult);
                Assert.True(okObjectResult.StatusCode == StatusCodes.Status200OK);

                var companies = okObjectResult.Value as List<Company>;
                Assert.NotNull(companies);
                Assert.True(companies.Count() == 3);
            }
        }

        [Fact]
        public async Task GetCompanyClaims()
        {
            using (var testSetup = new TestSetup())
            {
                var mockCompannyControllerLogger = new Mock<ILogger<CompaniesController>>();
                var companiesController = new CompaniesController(mockCompannyControllerLogger.Object, testSetup.mockCompanyService, testSetup.mockAutoMapper);

                var actionResult = await companiesController.GetCompanyClaims(1);
                Assert.NotNull(actionResult);

                var okObjectResult = actionResult as OkObjectResult;
                Assert.NotNull(okObjectResult);
                Assert.True(okObjectResult.StatusCode == StatusCodes.Status200OK);

                var companyClaims = okObjectResult.Value as List<Claim>;
                Assert.NotNull(companyClaims);
                Assert.True(companyClaims.Count() == 2);
            }
        }
    }
}