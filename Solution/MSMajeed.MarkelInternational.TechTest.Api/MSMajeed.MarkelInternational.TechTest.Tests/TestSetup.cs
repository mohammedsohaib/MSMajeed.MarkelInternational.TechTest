﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using MSMajeed.MarkelInternational.TechTest.Api;
using MSMajeed.MarkelInternational.TechTest.Data;
using MSMajeed.MarkelInternational.TechTest.Data.Models;
using MSMajeed.MarkelInternational.TechTest.Data.Services;

namespace MSMajeed.MarkelInternational.TechTest.Tests
{
    public class TestSetup : IDisposable
    {
        private DbContextOptions<TechTestDbContext>? mockDbContextOptions;
        public Mapper? mockAutoMapper;
        public CompanyService? mockCompanyService;
        public ClaimService? mockClaimService;

        private readonly List<Company> mockCompanies = new List<Company>()
        {
            new Company { Id = 1, Name = "PC World", InsuranceEndDate = DateTime.UtcNow.AddDays(365), Country = "United Kingdom", Active = true },
            new Company { Id = 2, Name = "Modern Banking", InsuranceEndDate = DateTime.UtcNow.AddDays(-30), Country = "Spain", Active = true },
            new Company { Id = 3, Name = "IMO Car Wash", InsuranceEndDate = DateTime.UtcNow.AddDays(30), Country = "Germany", Active = true }
        };

        private readonly List<ClaimType> mockClaimTypes = new List<ClaimType>()
        {
            new ClaimType { Id = 1, Name = "Fault" },
            new ClaimType { Id = 2, Name = "Non-Fault" }
        };

        private readonly List<Claim> mockClaims = new List<Claim>()
        {
            new Claim { Id = 1, CompanyId = 1, ClaimTypeId = 1, UCR = "Lost Data", ClaimDate = DateTime.UtcNow },
            new Claim { Id = 2, CompanyId = 1, ClaimTypeId = 2, UCR = "Hacked", ClaimDate = DateTime.UtcNow.AddDays(-90) },
            new Claim { Id = 3, CompanyId = 3, ClaimTypeId = 1, UCR = "Customer Car Damaged", ClaimDate = DateTime.UtcNow.AddDays(-365) }
        };

        public TestSetup()
        {
            var mockAutoMapperCongfiguration = new MapperConfiguration(cfg => cfg.AddProfile(new MappingProfiles()));
            mockAutoMapper = new Mapper(mockAutoMapperCongfiguration);

            mockDbContextOptions = new DbContextOptionsBuilder<TechTestDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var mockDbContext = new TechTestDbContext(mockDbContextOptions))
            {
                mockDbContext.Companies.AddRange(mockCompanies);
                mockDbContext.ClaimTypes.AddRange(mockClaimTypes); 
                mockDbContext.Claims.AddRange(mockClaims);
                mockDbContext.SaveChanges(); 
            }

            var mockDbContextFactory = new Mock<IDbContextFactory<TechTestDbContext>>();
            mockDbContextFactory.Setup(c => c.CreateDbContext()).Returns(() => new TechTestDbContext(mockDbContextOptions));

            var mockCompanyServiceLogger = new Mock<ILogger<CompanyService>>();
            mockCompanyService = new CompanyService(mockCompanyServiceLogger.Object, mockDbContextFactory.Object);

            var mockClaimServiceLogger = new Mock<ILogger<ClaimService>>();
            mockClaimService = new ClaimService(mockClaimServiceLogger.Object, mockDbContextFactory.Object);
        }
        

        public void Dispose()
        {
            var mockDbContext = new TechTestDbContext(mockDbContextOptions);
            mockDbContext.Database.EnsureDeleted();
            mockDbContext.Dispose();

            mockDbContextOptions = null;
            mockAutoMapper = null;
            mockCompanyService = null;
            mockClaimService = null;
        }
    }
}
