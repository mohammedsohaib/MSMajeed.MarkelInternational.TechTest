﻿using Microsoft.EntityFrameworkCore;
using MSMajeed.MarkelInternational.TechTest.Data.Models;

namespace MSMajeed.MarkelInternational.TechTest.Data
{
    public class TechTestDbContext : DbContext
    {
        public TechTestDbContext(DbContextOptions<TechTestDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>().HasData(
                new Company { Id = 1, Name = "PC World", InsuranceEndDate = DateTime.UtcNow.AddDays(365), Country = "United Kingdom", Active = true },
                new Company { Id = 2, Name = "Modern Banking", InsuranceEndDate = DateTime.UtcNow.AddDays(-30), Country = "Spain", Active = true },
                new Company { Id = 3, Name = "IMO Car Wash", InsuranceEndDate = DateTime.UtcNow.AddDays(30), Country = "Germany", Active = true });

            modelBuilder.Entity<ClaimType>().HasData(
                new ClaimType { Id = 1, Name = "Fault" },
                new ClaimType { Id = 2, Name = "Non-Fault" });

            modelBuilder.Entity<Claim>().HasData(
                new Claim { Id = 1, CompanyId = 1, ClaimTypeId = 1, UCR = "Lost Data", ClaimDate = DateTime.UtcNow },
                new Claim { Id = 2, CompanyId = 1, ClaimTypeId = 2, UCR = "Hacked", ClaimDate = DateTime.UtcNow.AddDays(-90) },
                new Claim { Id = 3, CompanyId = 3, ClaimTypeId = 1, UCR = "Customer Car Damaged", ClaimDate = DateTime.UtcNow.AddDays(-365) });
        }

        public DbSet<Claim> Claims { get; set; }
        public DbSet<ClaimType> ClaimTypes { get; set; }
        public DbSet<Company> Companies { get; set; }
    }
}
