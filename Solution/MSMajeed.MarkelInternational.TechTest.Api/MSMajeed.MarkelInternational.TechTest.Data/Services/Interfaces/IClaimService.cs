﻿using Microsoft.VisualBasic;
using MSMajeed.MarkelInternational.TechTest.Data.Models;

namespace MSMajeed.MarkelInternational.TechTest.Data.Services.Interfaces
{
    public interface IClaimService
    {
        Task<Claim> GetClaim(int claimId);
        Task<ClaimType> GetClaimType(int claimTypeId);
        Task UpdateClaim(Claim updatedClaim);
    }
}
