﻿using MSMajeed.MarkelInternational.TechTest.Data.Models;

namespace MSMajeed.MarkelInternational.TechTest.Data.Services.Interfaces
{
    public interface ICompanyService
    {
        Task<IEnumerable<Company>> GetCompanies();
        Task<Company> GetCompany(int companyId);
        Task<IEnumerable<Claim>> GetCompanyClaims(int companyId);
    }
}
