﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MSMajeed.MarkelInternational.TechTest.Data.Models;
using MSMajeed.MarkelInternational.TechTest.Data.Services.Interfaces;

namespace MSMajeed.MarkelInternational.TechTest.Data.Services
{
    public class CompanyService: ICompanyService
    {
        private readonly ILogger<CompanyService> _logger;
        private readonly IDbContextFactory<TechTestDbContext> _dbContextFactory;

        public CompanyService(ILogger<CompanyService> logger, IDbContextFactory<TechTestDbContext> dbContextFactory)
        {
            _logger = logger;
            _dbContextFactory = dbContextFactory;
        }

        public async Task<IEnumerable<Company>> GetCompanies()
        {
            using (var dbContext = _dbContextFactory.CreateDbContext())
            {
                return await dbContext.Companies.ToListAsync();
            }
        }

        public async Task<Company> GetCompany(int companyId)
        {
            using (var dbContext = _dbContextFactory.CreateDbContext())
            {
                return await dbContext.Companies.SingleOrDefaultAsync(company => company.Id == companyId);
            }
        }

        public async Task<IEnumerable<Claim>> GetCompanyClaims(int companyId)
        {
            using (var dbContext = _dbContextFactory.CreateDbContext())
            {
                return await dbContext.Claims.Where(claim => claim.CompanyId == companyId).ToListAsync();
            }  
        }
    }
}
