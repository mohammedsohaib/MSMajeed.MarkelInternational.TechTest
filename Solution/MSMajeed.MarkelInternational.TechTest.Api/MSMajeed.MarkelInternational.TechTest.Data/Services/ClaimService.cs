﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MSMajeed.MarkelInternational.TechTest.Data.Models;
using MSMajeed.MarkelInternational.TechTest.Data.Services.Interfaces;

namespace MSMajeed.MarkelInternational.TechTest.Data.Services
{
    public class ClaimService : IClaimService
    {
        private readonly ILogger<ClaimService> _logger;
        private readonly IDbContextFactory<TechTestDbContext> _dbContextFactory;

        public ClaimService(ILogger<ClaimService> logger, IDbContextFactory<TechTestDbContext> dbContextFactory)
        {
            _logger = logger;
            _dbContextFactory = dbContextFactory;
        }

        public async Task<Claim> GetClaim(int claimId)
        {
            using (var dbContext = _dbContextFactory.CreateDbContext())
            {
                return await dbContext.Claims.SingleOrDefaultAsync(claim => claim.Id == claimId);
            }
        }

        public async Task<ClaimType> GetClaimType(int claimTypeId)
        {
            using (var dbContext = _dbContextFactory.CreateDbContext())
            {
                return await dbContext.ClaimTypes.SingleOrDefaultAsync(claimType => claimType.Id == claimTypeId);
            }
        }

        public async Task UpdateClaim(Claim updatedClaim)
        {
            using (var dbContext = _dbContextFactory.CreateDbContext())
            {
                dbContext.Claims.Update(updatedClaim);
                await dbContext.SaveChangesAsync();
            }
        }
    }
}
