# Markel International Tech Test
## API Setup Guide
1. Install .NET 6. This has been chosen since it has LTS (Long Term Suport) from Microsoft
2. API is located under Solution\MSMajeed.MarkelInternational.TechTest.Api
3. Right click the MSMajeed.MarkelInternational.TechTest.Api project then click "Set as Statup Project"
4. Now hit start ensuring MSMajeed.MarkelInternational.TechTest.Api is selected and not IIS
5. Navigate to https://localhost:7264/swagger/index.html in a web browser for the API